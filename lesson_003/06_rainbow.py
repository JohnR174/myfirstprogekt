# -*- coding: utf-8 -*-

# (цикл for)

import simple_draw as sd

rainbow_colors = (sd.COLOR_RED, sd.COLOR_ORANGE, sd.COLOR_YELLOW, sd.COLOR_GREEN,
                  sd.COLOR_CYAN, sd.COLOR_BLUE, sd.COLOR_PURPLE)

# Нарисовать радугу: 7 линий разного цвета толщиной 4 с шагом 5 из точки (50, 50) в точку (350, 450)
# x = 50
# y = 350
#
# for color in rainbow_colors:
#     start = sd.get_point(x, 50)
#     finish = sd.get_point(y, 450)
#     sd.line(start_point=start, end_point=finish, color=color, width=4)
#     x = x + 5
#     y = y + 5

# Усложненное задание, делать по желанию.
# Нарисовать радугу дугами от окружности (cсм sd.circle) за нижним краем экрана,
# поэкспериментировать с параметрами, что бы было красиво
x = 20
y = 20
for color in rainbow_colors:
    x = x + 5
    y = y + 5
    sd.circle(color=color, width=4, center_position=sd.get_point(x, y), radius=500)

sd.pause()
