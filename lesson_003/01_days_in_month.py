# -*- coding: utf-8 -*-

# (if/elif/else)

# По номеру месяца вывести кол-во дней в нем (без указания названия месяца, в феврале 28 дней)
# Результат проверки вывести на консоль
# Если номер месяца некорректен - сообщить об этом

# Номер месяца получать от пользователя следующим образом
user_input = input("Введите, пожалуйста, номер месяца: ")
month = int(user_input)
print('Вы ввели', month)

calendar = ['January 31 day', 'february 28 day', 'March 31 day']
if month == 1:
    print(calendar[0])
elif month == 2:
    print(calendar[1])
elif month == 3:
    print(calendar[2])
else: print('Error')



# Вариант с циклом
user_input = input("Введите, пожалуйста, номер месяца: ")
month = int(user_input)
print('Вы ввели', month)

calendar = ['January 31 day', 'february 28 day', 'March 31 day']

i = month - 1
while -1 < i < len(calendar):
    mounth = calendar[i]
    print(mounth)
    if mounth == 'January 31 day':
        print('Ура, месяц найден!')
        break
    elif mounth == 'february 28 day':
        print('Ура, месяц найден!')
        break
    i += 1
else: print('error')
print('дотвиданя!')







