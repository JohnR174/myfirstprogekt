# -*- coding: utf-8 -*-
import simple_draw
import simple_draw as sd

# Нарисовать пузырек - три вложенных окружностей с шагом 5 пикселей
sd.resolution = (1200, 600)


# point = sd.get_point(100, 100)
# radius = 50
# for _ in range(3):
#     radius = radius + 5
#     sd.circle(center_position=point, radius=radius)

# Написать функцию рисования пузырька, принммающую 2 (или более) параметра: точка рисовании и шаг

# def bubble(point, step):
#     radius = 20
#     for _ in range(3):
#         radius += step
#         sd.circle(center_position=point, radius=radius, width=3)
#
#
# for x in range(100, 1100, 100):
#     point = sd.get_point(x, 250)
#     bubble(point, 10)

# Нарисовать 10 пузырьков в ряд
# def bubble(point, step):
#     radius = 20
#     for x in range(100, 1100, 100):
#         point = sd.get_point(x, 200)
#         radius += step
#         sd.circle(center_position=point, radius=radius, width=3)
#
#
# bubble(1, 0)

# Нарисовать три ряда по 10 пузырьков
# def bubble(point, step):
#     radius = 20
#     for _ in range(3):
#         radius += step
#         for x in range(100, 1100, 100):
#             for y in range(100, 400, 100):
#                 point = sd.get_point(x, y)
#                 sd.circle(center_position=point, radius=radius, width=3)
#
#
# bubble(1, 10)

# Нарисовать 100 пузырьков в произвольных местах экрана случайными цветами

radius = 20
for _ in range(100):
    point = sd.random_point()
    color = sd.random_color()
    radius = sd.random_number()
    sd.circle(center_position=point, radius=radius, color=color)


sd.pause()
