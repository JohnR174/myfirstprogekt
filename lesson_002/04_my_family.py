#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Создайте списки:

# моя семья (минимум 3 элемента, есть еще дедушки и бабушки, если что)
my_family = ['I', 'Oly', 'Barik']


# список списков приблизителного роста членов вашей семьи
my_family_height = [
    # ['имя', рост],
    ['I', 180],
    ['Oly', 160],
    ['Barik', 30]
]
# Выведите на консоль свой рост
#   Мой рост - ХХ см

my_height = my_family_height[0][1]
result1 = 'Мой рост ' + str(my_height) + ' см'
print(result1)
print((f"мой рост {my_height} см"))

# Выведите на консоль общий рост вашей семьи как сумму ростов всех членов
# Общий рост моей семьи - ХХ см

Sum = my_family_height[0][1]+my_family_height[1][1]+my_family_height[2][1]
result = 'Общий рост моей семьи ' + str(Sum) + ' см'
print(result)



my_family_height = {
    # ['имя', рост],
    'I': (180),
    'Oly': (160),
    'Barik': (30)
}
print(my_family_height ['I'] + my_family_height ['Oly'] + my_family_height ['Barik'])

