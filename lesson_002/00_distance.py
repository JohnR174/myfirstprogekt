#!/usr/bin/env python3
# -*- coding: utf-8 -*-

# Есть словарь координат городов
from pprint import pprint

sites = {
    'Moscow': (550, 370),
    'London': (510, 510),
    'Paris': (480, 480),
}

# Составим словарь словарей расстояний между ними
# расстояние на координатной сетке - корень из (x1 - x2) ** 2 + (y1 - y2) ** 2

distances = {}

Moscow = sites['Moscow']
London = sites['London']
Paris = sites['Paris']

Moscow_London = ((Moscow[0] - London[0]) ** 2 + (Moscow[1] - London[1]) ** 2) ** .5
Moscow_Paris = ((Moscow[0] - Paris[0]) ** 2 + (Moscow[1] - Paris[1]) ** 2) ** .5
Paris_London = ((Paris[0] - London[0]) ** 2 + (Paris[1] - London[1]) ** 2) ** .5

distances['Moscow'] = {}
distances['Moscow']['London'] = Moscow_London
distances['Moscow']['Paris'] = Moscow_Paris

distances['London'] = {}
distances['London']['Moscow'] = Moscow_London
distances['London']['Paris'] = Paris_London

distances['Paris'] = {}
distances['Paris']['Moscow'] = Moscow_Paris
distances['Paris']['London'] = Paris_London

pprint(distances)

# как работают вложенные словари
a = {}
a['b'] = {}
a['b']['c'] = 5
a['b']['e'] = 2
a['e'] = 4

print(a)