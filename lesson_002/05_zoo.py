#!/usr/bin/env python
# -*- coding: utf-8 -*-

# есть список животных в зоопарке

zoo = ['lion', 'kangaroo', 'elephant', 'monkey', ]

# посадите медведя (bear) между львом и кенгуру
#  и выведите список на консоль
zoo.insert(1, 'Bear')
print (zoo)

# добавьте птиц из списка birds в последние клетки зоопарка
birds = ['rooster', 'ostrich', 'lark', ]
#  и выведите список на консоль
zoo_birds=zoo+birds
print (zoo_birds)
# a = zoo.extend(birds)
# print(a)

# уберите слона
#  и выведите список на консоль
zoo_birds.pop(3)
print(zoo_birds)

# выведите на консоль в какой клетке сидит лев (lion) и жаворонок (lark).
# Номера при выводе должны быть понятны простому человеку, не программисту.

index_lion = zoo_birds.index('lion')
index_Lark = zoo_birds.index('lark')
print(enumerate(zoo_birds))


